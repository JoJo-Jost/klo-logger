FROM node:10-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ENV TZ='Europe/Berlin'

CMD [ "npm", "start" ]