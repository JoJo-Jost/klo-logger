const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const fsPromis = require('fs').promises;
const path = require('path');
const app = express();

const PORT = process.env.PORT || 80;
const LOG_DIR = process.env.LOG_DIR || `${__dirname}/logs/`;
const ACCESS_LOG_NAME = 'access.log';

app.use(express.json());

if (!fs.existsSync(LOG_DIR)){
    fs.mkdirSync(LOG_DIR);
}

const accessLogPath = path.join(LOG_DIR, ACCESS_LOG_NAME);
const accessLogStream = fs.createWriteStream(accessLogPath, { flags: 'a' });

const accessLogFormat = (tokens, req, res) => {
    if(tokens.url(req, res).match(/toilet/) && tokens.method(req, res) === 'PUT') {
        return [
            new Date().toString(),
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            JSON.stringify(req.body),
            '<br/>'
        ].join(' ')
    }
};

app.use(morgan(accessLogFormat, { stream: accessLogStream }));

app.put(
    '/toilet/:toiletId',
    (req, res) => res.json(req.body)
);

app.get(
    '/',
    async (req, res) => {
        try {
            const file = await fsPromis.readFile(accessLogPath);
            res.send(file.toString() ? file.toString() : 'No log entry yet..');
        } catch (e) {
            res.status(500).send(e)
        }
});

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
});