## Installation

1. build the image via
```bash
docker build -t jvoelker/klo-logger .
``` 

2. Start Server via Node:
```bash
docker run -it -d -p 3030:80 --rm --name klo-logger jvoelker/klo-logger
```

## Environment Variables

| Tables  | Are                                   |         
| ------- |:--------------------------------------:
| PORT    | Port for the Express Server to run on |
| LOG_DIR | Logging directory                     |


## API

To simulate a toilet status update perform a `PUT` Request on
`/toilet/{toiletId}`. `{toiletId}` represents the Id of the toilet. The request body is expected to match: 
```json
{
	"occupied": true
}
```
 